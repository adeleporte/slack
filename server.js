require('dotenv').config()
const Express = require('express')
const bodyParser = require('body-parser')
const { WebClient } = require('@slack/web-api');
const { IncomingWebhook } = require('@slack/webhook');
const { createMessageAdapter } = require('@slack/interactive-messages');
const { exec, spawn } = require( 'child_process' );

const dns = require('dns');
var ip = require('ip');

const {PORT, SLACK_TOKEN, SLACK_WEBHOOK_URL, SLACK_SIGNING_SECRET} = process.env

const port = PORT || 80
const token = SLACK_TOKEN;
const url = SLACK_WEBHOOK_URL;
const slackSigningSecret = SLACK_SIGNING_SECRET;
const slackInteractions = createMessageAdapter(slackSigningSecret);
const conversationId = 'CJH4F7Y8J';

const app = new Express()
app.use('/modals', slackInteractions.requestListener());
app.use(bodyParser.urlencoded({extended: true}))
const webhook = new IncomingWebhook(url);

const web = new WebClient(token);

var ansible_vars = {
    vcenter_user: String,
    vcenter_password: String,
    datacenter: String,
    dns_domain: String,
    ova_file: String,
    config: String,
    prefix: String
}


async function test() {
    webhook.send({
        text: 'App is starting...',
      });
}

async function view(trigger) {
    const result = await web.views.open({
        trigger_id: trigger,
        view: {
          type: 'modal',
          callback_id: 'view_identifier',
          title: {
            type: 'plain_text',
            text: 'NSX Deployment'
          },
          submit: {
            type: 'plain_text',
            text: 'Submit'
          },
          "blocks": [
            {
              "type": "input",
              "block_id": "cPodName",
              "label": {
                "type": "plain_text",
                "text": "cPod Name (without cPod-)"
              },
              "element": {
                "type": "plain_text_input",
                "action_id": "cPodName",
                "placeholder": {
                  "type": "plain_text",
                  "text": "Type in here"
                },
                "multiline": false
              },
              "optional": false
            },
            {
                "type": "input",
                "block_id": "cPodPassword",
                "label": {
                  "type": "plain_text",
                  "text": "cPod Password"
                },
                "element": {
                  "type": "plain_text_input",
                  "action_id": "cPodPassword",
                  "placeholder": {
                    "type": "plain_text",
                    "text": "Type in here"
                  },
                  "multiline": false
                },
                "optional": false
            },
            {
                "type": "input",
                "block_id": "version",
                "element": {
                    "type": "static_select",
                    "action_id": "version",
                    "placeholder": {
                        "type": "plain_text",
                        "text": "Select a version",
                        "emoji": true
                    },
                    "initial_option": {
                        "text": {
                            "type": "plain_text",
                            "text": "v 2.5.1 Build 15314292",
                            "emoji": true
                        },
                        "value": "nsx-unified-appliance-2.5.1.0.0.15314292.ova"
                    },
                    "options": [{
                        "text": {
                            "type": "plain_text",
                            "text": "v 2.5.0 Build 14436426",
                            "emoji": false
                        },
                        "value": "nsx-unified-appliance-2.5.0.0.0.14436426.ova"
                    },
                    {
                        "text": {
                            "type": "plain_text",
                            "text": "v 2.5.1 Build 15314292",
                            "emoji": true
                        },
                        "value": "nsx-unified-appliance-2.5.1.0.0.15314292.ova"
                    },
                    {
                        "text": {
                            "type": "plain_text",
                            "text": "v 3.0.0 Build 15263052",
                            "emoji": true
                        },
                        "value": "nsx-unified-appliance-3.0.0.0.0.15263052.ova"
                    }
                    ]
                },
                "label": {
                    "type": "plain_text",
                    "text": "Version",
                    "emoji": true
                }
            },
            {
                "type": "input",
		"block_id": "config",
                "label": {
                  "type": "plain_text",
                  "text": "Config"
                },
                "element": {
                  "type": "radio_buttons",
                  "action_id": "config",
                  "initial_option": {
                    "value": "NONE",
                    "text": {
                      "type": "plain_text",
                      "text": "No Config"
                    }
                  },
                  "options": [
                    {
                      "value": "NONE",
                      "text": {
                        "type": "plain_text",
                        "text": "No Config"
                      }
                    },
                    {
                      "value": "BASIC",
                      "text": {
                        "type": "plain_text",
                        "text": "Basic Config (TZ, Host Preparation)"
                      }
                    },
                    {
                        "value": "FULL",
                        "text": {
                          "type": "plain_text",
                          "text": "Full Config (Basic + T0)"
                        }
                      }
                  ]
                }
              }
          ]
        }})
}

var log = ''

function getLog() {
    return log
}

function setLog(text) {
    log = text
}

async function ansiblePlaybook() {
    const result = await web.chat.postMessage({
        text: 'Start deploying',
        channel: conversationId,
        });

    var log = 'Start deploying'
    const intervalObj = setInterval(() => {
        //console.log(`${getLog()}`)
        web.chat.update({
            text: getLog(),
            channel: conversationId,
            ts: result.ts
          });
    }, 2000);
  
    var extra_vars = JSON.stringify(ansible_vars)
    command = spawn( '/usr/bin/ansible-playbook', ['/root/slack/ansible/deploy_nsx.yml', '--extra-vars', `${extra_vars}`, '-i', '/root/slack/ansible/hosts/hosts.ini' ] );
    console.log('/usr/bin/ansible-playbook' + ' /root/slack/ansible/deploy_nsx.yml' + ' --extra-vars ' + `"${extra_vars}"`)

    command.stdout.on( 'data', data => {
        console.log( `stdout: ${data}` );
        setLog(`stdout: ${data}`)
        
        /*
        webhook.send({
            text: `stdout: ${data}`,
          });
          */
    } );
    
    command.stderr.on( 'data', data => {
        console.log( `stderr: ${data}` );
        webhook.send({
            text: `stderr: ${data}`,
          });
    } );
    
    command.on( 'close', code => {
        console.log( `child process exited with code ${code}` );
        clearTimeout(intervalObj);
        if (code == 0) {
            webhook.send({
                text: `Nsx Manager has been deployed. The URL is https://${ansible_vars.ip_address}`,
              });
        } else { 
            webhook.send({
                text: `child process exited with code ${code}`,
              });
        }
    } );
}

app.post('/log', (req, res) => {
    console.log(req.body)

    //ls(req.body.text)
    view(req.body.trigger_id)

    return res.json({text: 'Handling the request...'})
  })


slackInteractions.viewSubmission('view_identifier', (payload) => {
    // Log the input elements from the view submission.
    //console.log(payload.view.state.values.version.version.selected_option.value);
    //console.log(payload.view.state.values);


   var buf = new Buffer.alloc(4)	
   dns.lookup(`vcsa.cpod-${payload.view.state.values.cPodName.cPodName.value}.az-demo.shwrfr.com`, (err, address, family) => {
	   buf = ip.toBuffer(address)
	   console.log(buf[0])
	   console.log(buf[1])
	   console.log(buf[2])
	   console.log(buf[3])
	   console.log('address: %j family: IPv%s', address, family);
	   var prefix = `${buf[0]}.${buf[1]}.${buf[2]}`

	   ansible_vars = {
             vcenter_username: `administrator@cpod-${payload.view.state.values.cPodName.cPodName.value}.az-demo.shwrfr.com`,
             vcenter_password: payload.view.state.values.cPodPassword.cPodPassword.value,
             dns_domain: `cpod-${payload.view.state.values.cPodName.cPodName.value}.az-demo.shwrfr.com`,
             ova_file: payload.view.state.values.version.version.selected_option.value,
             vm_name: "nsxt-ansible",
             datacenter: `cPod-${payload.view.state.values.cPodName.cPodName.value}`,
	     config: payload.view.state.values.config.config.selected_option.value,
             prefix: prefix
           }

	   console.log(ansible_vars)

	   // Start the Ansible Playbook
	   ansiblePlaybook()
           .then(() => {
             console.log('then') })
           .catch((error) => {
             console.log('error')
             console.log(error) })
           });
})


app.listen(port, () => {
    console.log(`Server started at localhost:${port}`)
    test();
})


